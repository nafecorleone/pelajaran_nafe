<!DOCTYPE html>
<html>
<head>
<style>


table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: center;    
}
</style>
</head>
<body>

<h2>HTML Table Lesson</h2>
<p>Test ngerjain PR by: Dimas</p>

<table style="width:50%">
  <tr>
    <th rowspan="2">Kode Barang</th>
    <th rowspan="2">Nama Barang</th>
    <th colspan="2">Stok Gudang</th>
  </tr>
  <tr>
    <th>Gudang A</th>
    <th>Gudang B</th>
  </tr>

  <tr>
    <td>B002</td>
    <td>Sepatu</td>
    <td>50</td>
    <td>3</td>
  </tr> 
  <tr>
    <td>C001</td>
    <td>Sandal</td>
    <td>30</td>
    <td>15</td>
  </tr> 
  <tr>
    <td>C002</td>
    <td>Celana</td>
    <td>5</td>
    <td>6</td>
  </tr> 
  <tr>
    <td>D001</td>
    <td>Payung</td>
    <td>1</td>
    <td>4</td>
  </tr>  
</table>

</body>
</html>
