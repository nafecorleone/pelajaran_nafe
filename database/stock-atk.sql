-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2018 at 03:10 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stock-atk`
--

-- --------------------------------------------------------

--
-- Table structure for table `list-item`
--

CREATE TABLE `list-item` (
  `no` int(11) NOT NULL,
  `nama_item` varchar(64) NOT NULL,
  `stock_awal` int(11) NOT NULL,
  `item_masuk` int(11) NOT NULL,
  `item_keluar` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `divisi` varchar(64) NOT NULL,
  `harga` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list-item`
--

INSERT INTO `list-item` (`no`, `nama_item`, `stock_awal`, `item_masuk`, `item_keluar`, `stock`, `tanggal`, `divisi`, `harga`, `keterangan`) VALUES
(1, 'Amplop Coklat Polos', 5, 1, 1, 5, '2018-08-01', 'test', 50000, 'asdqwer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `list-item`
--
ALTER TABLE `list-item`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `list-item`
--
ALTER TABLE `list-item`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
